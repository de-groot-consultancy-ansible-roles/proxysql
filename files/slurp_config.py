#!/usr/bin/env python3

import argparse
import pymysql
import sys
import yaml
from collections import OrderedDict

def represent_dictionary_order(self, dict_data):
    return self.represent_mapping('tag:yaml.org,2002:map', dict_data.items())

yaml.add_representer(OrderedDict, represent_dictionary_order)

class OrderedDictCursor(pymysql.cursors.DictCursorMixin, pymysql.cursors.Cursor):
    dict_type = OrderedDict

parser=argparse.ArgumentParser(description="ProxySQL admin configuration slurper (YML output)")

parser.add_argument('--user', help="ProxySQL admin username", default="proxysql-admin")
parser.add_argument('--host', help="ProxySQL admin host", default="127.0.0.1")
parser.add_argument('--password', help="ProxySQL admin password")
parser.add_argument('--port', help="ProxySQL admin port", default=6032, type=int)
parser.add_argument('--output-type', choices=["list", "dict"], help="Output type (list or dict)", default="list")
parser.add_argument('--object', required=True, help="Yaml object to which the query output becomes a list")
parser.add_argument('--query', required=True, help="ProxySQL admin query")

args = parser.parse_args()

print("Connecting to proxysql admin interface using mysql protocol", file=sys.stderr)
db = pymysql.connect(
    host=args.host,
    port=args.port,
    user=args.user,
    password=args.password,
    cursorclass=OrderedDictCursor
)

print(f"Executing query: {args.query}", file=sys.stderr)

with db.cursor() as cursor:
    cursor.execute(args.query)

    result = OrderedDict({args.object: []})


    for row in cursor.fetchall():
        result[args.object].append(row)

print("------------------------------------------------------------------------------\n\n", file=sys.stderr)

yaml.dump(result, sys.stdout, width=120, indent=4, default_flow_style=False)

print("\n\ndone! You can execute this script with stderr output redirection (2>/dev/null) to only get the yml returned and use that in batch processing.", file=sys.stderr)
