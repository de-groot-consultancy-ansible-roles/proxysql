# ProxySQL

Currently this role is compatible with ProxySQL v2

## Requirements

Ansible 2.5 minimum

## Configuration management

This role supports configuration management. When the configuration file is changed, the SQLite file with the running configuration is removed and ProxySQL is restarted.

Adding/changing a user will trigger a configuration change (which will trigger a ProxySQL restart). Pull requests are welcome to improve this.

## Graceful switchover

This role supports graceful switchovers. All health check services are brought down, then the role waits for a configured amount of time (10 seconds by default) and then restarts ProxySQL.

If your application requires longer for a graceful swithcover, please configure _proxysql_graceful_shutdown_time_ higher. 

## Notable variables

The variables that can be passed to this role. For all variables, take
a look at [defaults/main.yml](defaults/main.yml).

- proxysql_mysql_max_connections: 2048 - Maximum number of connections to the entire instance
- proxysql_mysql_default_query_timeout: 36000000  - Queries taking more then 10 hours will be killed
- proxysql_mysql_threads: 4  - Amount of threads ProxySQL has for processing queries
- proxysql_mysql_server_version: '10.11-MariaDB-log' - Server version ProxySQL reports to the clients (10.11 is substituted with mariadb_version if it is defined elsewhere)
- proxysql_mysql_monitor_username: 'monitor' - Username used for monitoring backend server
- proxysql_mysql_monitor_password: 'monitor' - Password  used for monitoring backend server

## Servers, users, host groups configuration

All list configuration items accept all parameters. Unknown parameters will be passed as-is in the ProxySQL configuration file. If incorrect parameters are passed, ProxySQL might not start.

Example YML variables:
```yaml
# MySQL servers, check: https://proxysql.com/documentation/main-runtime/#mysql_servers
proxysql_mysql_servers: 
 - hostname: "127.0.0.1"    # no default, required . If port is 0, hostname is interpred as a Unix Socket domain
   port: 3306               # Default: 3306. If port is 0, unix socket login
   hostgroup_id: 0          # no default, required
   status: "ONLINE"         # default: ONLINE
   weight: 1                # default: 1
   max_replication_lag: 10  # default 0 . If greater than 0 and replication lag passes such threshold, the server is shunned
   max_connections: 200     # Default 1000. When this is reached, the server will not receive more connections despite it's weight.

# defines all the MySQL users, check: https://proxysql.com/documentation/main-runtime/#mysql_users
proxysql_mysql_users: []
 - username: "username"    # no default , required
   password: "password"    # default: ''
   default_hostgroup: 0    # default: 0
   active: 1               # default: 1
   use_ssl: 0              # default 0
   max_connections: 1000   # default 1000
   default_schema: "test"  # no default

# defines MySQL Query Rules. Check https://proxysql.com/documentation/main-runtime/#mysql_query_rules
proxysql_mysql_query_rules:
 - rule_id: 1
   active: 1
   match_pattern: "^SELECT .* FOR UPDATE$"
   destination_hostgroup: 0
   apply: 1

 - rule_id: 2
   active: 1
   match_pattern: "^SELECT"
   destination_hostgroup: 1
   apply: 1

# Defines Scheduler for custom checker scripts. Check: https://proxysql.com/documentation/main-runtime/#scheduler
proxysql_scheduler: 
 - id: 1
   active: 0
   interval_ms: 10000
   filename: "/var/lib/proxysql/proxysql_galera_checker.sh"
   args:
    - "0"
    - "1"
    - "0"
    - "1"
    - "/var/lib/proxysql/proxysql_galera_checker.log"

# Ascynhronous replication host groups. Check: https://proxysql.com/documentation/main-runtime/#mysql_replication_hostgroups
proxysql_mysql_replication_hostgroups:
 - writer_hostgroup: 30
   reader_hostgroup: 40
   comment: "test repl 1"
                
 - writer_hostgroup: 50
   reader_hostgroup: 60
   comment: "test repl 2"

# Galera host groups. Check: https://proxysql.com/documentation/main-runtime/#mysql_galera_hostgroups
# NOTE: Add your servers in the reader hostgroup, ProxySQL will take care of everything else.
proxysql_mysql_galera_hostgroups: []
 - writer_hostgroup: 60
   backup_writer_hostgroup: 61
   reader_hostgroup: 70
   offline_hostgroup: 79
   health_check_enabled: True
   health_check_port: 9200
   health_check_type: galera
   comment: "Galera cluster 1"
                
 - writer_hostgroup: 80
   backup_writer_hostgroup: 81
   reader_hostgroup: 90
   offline_hostgroup: 99
   comment: "Galera cluster 2"
```

## Migrate from manually managed instance
This role has a feature implemented to easily and safely migrate from a manually managed running ProxySQL instance. You can use the special migration feature to get the running configuration for proxysql. You cannot copy/paste the variables into the role's configuration, but all other configurations should be possible to copy/paste. 

For the variables we recommend to go through the variables the role supports, and configure those that have a different configuration in your running system. If other configurations (that the role does not yet support to change) are relevant to have non-default, pull requests and/or reported issues are more then welcome.

All other configurations (mysql_servers, mysql_galera_hostgroups, mysql_query_rules, etc) you should be able to just copy/paste them into your group variables or playbook. Possibly not everything is supported yet (check what the role writes into /etc/proxysql.cnf) for which pull requests and/or reported issues are more then welcome as well.

Use these parameters to run the migration check mode:
```yaml
proxysql_migration_check_mode: True
proxysql_migration_login_host: "127.0.0.1"
proxysql_migration_login_port: "6032"
proxysql_migration_login_user: "admin"
proxysql_migration_login_password: "old-admin-password"
```

We intentionally don't share the variable for the proxysql admin password because many people set up an unsafe password, and the role will regenerate a safe password with every time it runs. Running the migration check mode installs the python3-PyMySQL package on the target server that might trigger package configuration on debian based systems.


## Install from link (Supports both RedHat/Debian)
Example:
```yaml
proxysql_install_from_official_repo: false
proxysql_install_from_unofficial_url: "https://github.com/sysown/proxysql/releases/download/v2.0.18/proxysql_2.0.18-debian10_amd64.deb"
```

## Enable SSL

```yaml
proxysql_ssl: True

# If enabled, the role will deploy server certificates specified and use them for incoming connections.
proxysql_ssl_c2p_enabled: False
proxysql_ssl_c2p_ca:   "{{ lookup('file', 'ssl/ca-certificate.crt') }}"
proxysql_ssl_c2p_cert: "{{ lookup('file', 'ssl/server-' + inventory_hostname + '.crt') }}"
proxysql_ssl_c2p_key:  "{{ lookup('file', 'ssl/server-' + inventory_hostname + '.key') }}"


proxysql_ssl_p2s_enabled: True
proxysql_ssl_p2s_ca:   "{{ lookup('file', 'ssl/ca-certificate.crt') }}"
proxysql_ssl_p2s_cert: "{{ lookup('file', 'ssl/client-' + inventory_hostname + '.crt') }}"
proxysql_ssl_p2s_key:  "{{ lookup('file', 'ssl/client-' + inventory_hostname + '.key') }}"
```

## Tags support
Several tags are implemented, but not all of them are guaranteed to work independently. For example, the health check implementation depends on a working admin CLI client. These tags are supported:
- migration_check_mode: When check mode is enabled the playbooks stops execution after fetching the configuration, this tag is for extra safety.
- installation_and_upgrades: Install, upgrade and downgrade ProxySQL. The version is locked using yum-versionlock or dpkg-selections.
- certificate_management: Deploy TLS certificates
- configuration_management: Deploy templated configuration file
- health_check: Install health check services
- admin_client: Installs the MariaDB client and deploys configuration for auto login to ProxySQL
- os_management: Configure firewalld, install all packages, reboot if necesarry
- restart_if_needed: Restart ProxySQL or reboot the instance if needed (configuration change, certificate change, new kernel installed)

## Example Playbook

Add your variables in group_vars/proxysql.yml
```yaml
 - hosts: proxysql
   roles:
   - proxysql
```



## Migration check mode

Migration check mode fetches the currently running configuration so you can add this into your group_vars, and then use the role to install/upgrade proxysql on other instances.
```yaml
- name: "ProxySQL migration check mode"
  hosts: old_proxysql
  become: true
  roles:
   - role: proxysql
     proxysql_migration_check_mode: True
     proxysql_migration_login_user: "admin"
     proxysql_migration_login_password: "admin"
     #proxysql_migration_configuration_slurper_cleanup: False
```



## Health check

This role installs a HTTP health check for galera host groups. By default this runs on port 9200, if you configure multiple galera hostgroups with this role you need to specify a different port.
Contributions to install health checks for other hostgroups are appreciated.

## Health check status cache

This role creates a small ram disk in which the health check result is cached for a few seconds. This means it is safe to call the health check from hundreds of application servers.

## Contributors

Contributions are more then welcome in the form of pull requests.

Michaël de Groot (maintainer)
Julien Bunel originally created the role on github (https://github.com/jubunel/proxysql)
Sarvesh Goburdhun
