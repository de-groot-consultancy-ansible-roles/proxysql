## Contributions
Contributions are greatly appreciated. You can contribute in many ways:
- Ansible code
- Bug reports
- Testing

## License 
While the role is released as AGPL v3, contributions should be offered with new-BSD 3 clause license. This allows us to redistribute your changes as AGPL licensed or with other licenses. Your contributions to our open source project will be free to use (distributed under the AGPL license) for everybody.

Please state clearly (while submitting your merge request) that the code is offered under the 3-clause new-BSD license.